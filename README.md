
# Sofram Image Crop API
The soul purpose of this api is to create a resized version of an already existing image and a thumbnail for it in a Lamda function. The flow is starting with a request to Aws Lamda functio through Aws Api Gateway and in the function a resized and thumbnail version of an existing image are created and saved back to S3 bucket and returns the newly created images locations.

The possible way of utilizing this api is to call it for to create resized and thumbnail versions of a menu item's photo uploaded by a vendor in food ordering service. 

## Tech

Sofram-Notification-Api uses a number of AWS cloud techs to work properly:

- [S3] - Aws S3 Bucket; used for storing images
- [Lamda] - Aws Lamda for running code serverless without provisioning or managing servers.
- [Api Gateway] - Used in front of the lamda function to be able to call the lamda function in a REST manner
- [LocalStack] - Cloud emulator for AWS services, used to test various AWS services locally
- [docker-compose] - Used for to be able to run multiple services locally like s3, lamda, api gateway

## Installation

Api requires [Golang](https://go.dev/) v1.18+ to run.

Clone the api somewhere in your computer.

```sh
cd sofram-image-processor
```

Serves Api Gateway, S3 and Lamda services locally with Localstack
```sh
docker-compose up
```

Creates a S3 bucket, Api Gateway and Lamda function with main.go
Also uploads an image to the newly created bucket
```sh
sh local_setup.sh
```

## Testing

After running  'sh local_setup.sh' at the end of the terminal among the outputs there should be a message something like 'API available at: http://localhost:4566/restapis/20czeta64l/test/_user_request_/put_full_image_name_here'

If you change the url part 'put_full_image_name_here' with the fullname of the image uploaded after shell script run and make a get request like

```sh
curl -X GET "http://localhost:4566/restapis/20czeta64l/test/_user_request_/20210806_193450.jpeg"
```
you should get a response like 

```sh
{
    "utc": "2022-06-10T12:36:18.0035176Z",
    "resizedLocation": "http://localstack:4566/local-aws-demo/resized/20210806_193450.jpeg",
    "thumbnailLocation": "http://localstack:4566/local-aws-demo/thumbnail/20210806_193450.jpeg"
}
```

Then try the urls by just changing the 'localstack' segment with 'localhost' because we can reach S3 bucket only from localstack container's external address and it is "http://localhost:4566" but when we tried to reach S3 bucket from our lamda function it used 'localstack' and it is the service name we specified in docker-compose.


To stop the service and deleting volumes 
```sh
docker-compose down -v
```


