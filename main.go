package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aydinmuzaffer/sofram-image-processor/cloud"
	"github.com/aydinmuzaffer/sofram-image-processor/cropping"
)

type response struct {
	UTC               time.Time `json:"utc"`
	ResizedLocation   string    `json:"resizedLocation"`
	ThumbnailLocation string    `json:"thumbnailLocation"`
}

var (
	s3Handler       cloud.S3Bucket
	cropHandler     cropping.CropHandler
	originalFolder  string
	resizedFolder   string
	thumbNailFolder string
)

func init() {
	originalFolder = os.Getenv("ORIGINAL_FOLDER")
	resizedFolder = os.Getenv("RESIZED_FOLDER")
	thumbNailFolder = os.Getenv("THUMBNAIL_FOLDER")
	bucket := os.Getenv("S3_BUCKET")

	sess, err := cloud.NewAwsSession()
	if err != nil {
		log.Fatalf("Cannot load the AWS configs: %s", err)
	}
	s3Handler = cloud.NewS3Handler(sess, bucket)

	cropHandler = cropping.NewCropHandler()
}

func handleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	key := request.PathParameters["key"]
	originalKey := s3Handler.GetS3Key(originalFolder, key)
	exists, originalImage, err := s3Handler.DownloadImage(ctx, originalKey)

	if !exists {

		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("notfound error with key: %s %s", key, err.Error())
	}

	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("download error with key: %s", key)
	}

	resizedKey := s3Handler.GetS3Key(resizedFolder, key)
	resizedImage, err := cropHandler.Crop(250, 300, originalImage)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("resize error with key: %s", err.Error())
	}
	resizedRes, err := s3Handler.UploadImage(ctx, resizedKey, resizedImage)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("resized upload error with key: %s", resizedKey)
	}

	thumbnailKey := s3Handler.GetS3Key(thumbNailFolder, key)
	thumbnaiImage, err := cropHandler.Thumbnail(150, 150, originalImage)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("thumbnail error with key: %s", err.Error())
	}
	thumbnailRes, err := s3Handler.UploadImage(ctx, thumbnailKey, thumbnaiImage)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 500}, fmt.Errorf("thumbnail upload error with key: %s", thumbnailKey)
	}

	now := time.Now()
	resp := &response{
		UTC:               now.UTC(),
		ThumbnailLocation: thumbnailRes.Location,
		ResizedLocation:   resizedRes.Location,
	}
	body, err := json.Marshal(resp)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	return events.APIGatewayProxyResponse{Body: string(body), StatusCode: 200}, nil
}

func main() {
	lambda.Start(handleRequest)
}
