package cropping

import (
	"bytes"
	"image"
	"image/jpeg"

	"github.com/nfnt/resize"
)

type CropHandler interface {
	Crop(height, width uint, imgByte []byte) ([]byte, error)
	Thumbnail(height, width uint, imgByte []byte) ([]byte, error)
}

type cropHandler struct{}

func NewCropHandler() CropHandler {
	return &cropHandler{}
}

func (*cropHandler) Crop(height, width uint, imgByte []byte) ([]byte, error) {
	img, _, err := image.Decode(bytes.NewReader(imgByte))
	if err != nil {
		return imgByte, err
	}

	cropped := resize.Resize(width, height, img, resize.Lanczos3)
	buf := new(bytes.Buffer)
	err = jpeg.Encode(buf, cropped, nil)
	if err != nil {
		return imgByte, err
	}
	return buf.Bytes(), nil
}

func (*cropHandler) Thumbnail(height, width uint, imgByte []byte) ([]byte, error) {
	img, _, err := image.Decode(bytes.NewReader(imgByte))
	if err != nil {
		return imgByte, err
	}

	cropped := resize.Thumbnail(width, height, img, resize.Lanczos3)
	buf := new(bytes.Buffer)
	err = jpeg.Encode(buf, cropped, nil)
	if err != nil {
		return imgByte, err
	}
	return buf.Bytes(), nil
}
