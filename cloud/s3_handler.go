package cloud

import (
	"bytes"
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type S3Bucket interface {
	DownloadImage(ctx context.Context, key string) (bool, []byte, error)
	UploadImage(ctx context.Context, key string, data []byte) (*s3manager.UploadOutput, error)
	GetS3Key(folder string, key string) string
}

type s3Handler struct {
	Session *session.Session
	Bucket  string
}

func NewS3Handler(sess *session.Session, bucket string) S3Bucket {
	return &s3Handler{
		Session: sess,
		Bucket:  bucket,
	}
}

func (s *s3Handler) DownloadImage(ctx context.Context, key string) (bool, []byte, error) {
	srv := s3.New(s.Session)
	_, err := srv.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return false, nil, fmt.Errorf("HeadObjectInputError: %s with Key: %s and Bucket: %s", err.Error(), key, s.Bucket)
	}
	//Download image from s3
	buffer := &aws.WriteAtBuffer{}
	downloader := s3manager.NewDownloader(s.Session)
	_, err = downloader.DownloadWithContext(ctx, buffer, &s3.GetObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return true, nil, fmt.Errorf(fmt.Sprintf("could not download image from S3: %v", err))
	}
	return true, buffer.Bytes(), nil
}

func (s *s3Handler) UploadImage(ctx context.Context, key string, data []byte) (*s3manager.UploadOutput, error) {
	uploader := s3manager.NewUploader(s.Session)
	output, err := uploader.UploadWithContext(ctx, &s3manager.UploadInput{
		Bucket:      aws.String(s.Bucket),
		Key:         aws.String(key),
		ContentType: aws.String("image/jpeg"),
		Body:        bytes.NewReader(data),
	})
	if err != nil {
		return nil, err
	}
	return output, nil
}

func (s *s3Handler) GetS3Key(folder string, key string) string {
	return fmt.Sprintf("%s/%s", folder, key)
}
