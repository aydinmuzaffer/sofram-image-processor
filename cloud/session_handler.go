package cloud

import (
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func NewAwsSession() (*session.Session, error) {
	env := os.Getenv("ENV")
	profile := os.Getenv("AWS_PROFILE")
	endpoint := os.Getenv("AWS_ENDPOINT")
	region := os.Getenv("AWS_REGION")

	awsConf := aws.Config{}

	if env == "test" && endpoint != "" {
		awsConf.Endpoint = aws.String(endpoint)
		awsConf.S3ForcePathStyle = aws.Bool(true)
	} else {
		awsConf.Credentials = credentials.NewSharedCredentials("", profile)
		awsConf.Region = aws.String(region)
	}

	opts, err := session.NewSessionWithOptions(session.Options{
		Config:            awsConf,
		SharedConfigState: session.SharedConfigEnable,
	})

	if err != nil {
		return &session.Session{}, err
	}

	sess := session.Must(opts, nil)
	return sess, nil

}
