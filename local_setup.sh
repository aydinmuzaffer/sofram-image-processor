#!/bin/sh
set -x

LAMBDA_NAME=local-test
API_NAME=local-test
REGION=us-east-1
STAGE=test
S3_BUCKET=local-aws-demo
ORIGINAL_FOLDER=original
RESIZED_FOLDER=resized
THUMBNAIL_FOLDER=thumbnail

function fail() {
    echo $2
    exit $1
}

alias laws="awslocal --endpoint-url=http://localhost:4566"

echo "Creating S3 bucket: $S3_BUCKET"
laws s3 mb s3://${S3_BUCKET}

[ $? == 0 ] || fail 1 "Failed: AWS / bucket / create-bucket"

echo "Creating $ORIGINAL_FOLDER folder"
laws s3api put-object --bucket ${S3_BUCKET} --key ${ORIGINAL_FOLDER}/

echo "Uploading a photo to bucket"
laws s3 cp 20210806_193450.jpeg s3://${S3_BUCKET}/${ORIGINAL_FOLDER}/ 
[ $? == 0 ] || fail 7 "Failed: AWS / s3 / saving photo"


echo "Building lambda deployment package"
GOOS=linux go build -o ./main .
zip deployment.zip main; rm main

echo "Deploying lambda"
laws lambda create-function \
        --region ${REGION} \
        --function-name ${LAMBDA_NAME} \
        --runtime go1.x \
        --handler main \
        --memory-size 128 \
        --zip-file fileb://deployment.zip \
        --role arn:aws:iam::000000000000:role/irrelevant \
        --environment \
            "{\"Variables\":
                {
                    \"AWS_REGION\": \"${REGION}\",
                    \"AWS_ENDPOINT\": \"http://localstack:4566\",
                    \"S3_BUCKET\": \"${S3_BUCKET}\",
                    \"ENV\": \"${STAGE}\",
                    \"ORIGINAL_FOLDER\": \"${ORIGINAL_FOLDER}\",
                    \"RESIZED_FOLDER\": \"${RESIZED_FOLDER}\",
                    \"THUMBNAIL_FOLDER\": \"${THUMBNAIL_FOLDER}\"
                }
            }"
[ $? == 0 ] || fail 1 "Failed: AWS / lambda / create-function"

LAMBDA_ARN=$(laws lambda list-functions --query "Functions[?FunctionName==\`${LAMBDA_NAME}\`].FunctionArn" --output text --region ${REGION})

laws apigateway create-rest-api --region ${REGION} --name ${API_NAME}

[ $? == 0 ] || fail 2 "Failed: AWS / apigateway / create-rest-api"

API_ID=$(laws apigateway get-rest-apis --query "items[?name==\`${API_NAME}\`].id" --output text --region ${REGION})
echo "API_ID ${API_ID}"
PARENT_RESOURCE_ID=$(laws apigateway get-resources --rest-api-id ${API_ID} --query 'items[?path==`/`].id' --output text --region ${REGION})
echo "PARENT_RESOURCE_ID ${PARENT_RESOURCE_ID}"

laws apigateway create-resource \
    --region ${REGION} \
    --rest-api-id ${API_ID} \
    --parent-id ${PARENT_RESOURCE_ID} \
    --path-part "{key}"

[ $? == 0 ] || fail 3 "Failed: AWS / apigateway / create-resource"

RESOURCE_ID=$(laws apigateway get-resources --rest-api-id ${API_ID} --query 'items[?path==`/{key}`].id' --output text --region ${REGION})
echo "RESOURCE_ID ${RESOURCE_ID}"

laws apigateway put-method \
    --region ${REGION} \
    --rest-api-id ${API_ID} \
    --resource-id ${RESOURCE_ID} \
    --http-method GET \
    --request-parameters "method.request.path.key=true" \
    --authorization-type "NONE" \

[ $? == 0 ] || fail 4 "Failed: AWS / apigateway / put-method"

laws apigateway put-integration \
    --region ${REGION} \
    --rest-api-id ${API_ID} \
    --resource-id ${RESOURCE_ID} \
    --http-method GET \
    --type AWS_PROXY \
    --integration-http-method POST \
    --uri arn:aws:apigateway:${REGION}:lambda:path/2015-03-31/functions/${LAMBDA_ARN}/invocations \
    --passthrough-behavior WHEN_NO_MATCH \

[ $? == 0 ] || fail 5 "Failed: AWS / apigateway / put-integration"

laws apigateway create-deployment \
    --region ${REGION} \
    --rest-api-id ${API_ID} \
    --stage-name ${STAGE} \

[ $? == 0 ] || fail 6 "Failed: AWS / apigateway / create-deployment"

ENDPOINT=http://localhost:4566/restapis/${API_ID}/${STAGE}/_user_request_/put_full_image_name_here

echo "API available at: ${ENDPOINT}"

set +x